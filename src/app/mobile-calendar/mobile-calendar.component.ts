import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { addDays, addMonths } from 'date-fns';
import { animate, query, style, transition, trigger } from '@angular/animations';
import { FormGroup } from '@angular/forms';

export const MONTH_ENUM = {
  0: 'Январь',
  1: 'Февраль',
  2: 'Март',
  3: 'Апрель',
  4: 'Май',
  5: 'Июнь',
  6: 'Июль',
  7: 'Август',
  8: 'Сентябрь',
  9: 'Октябрь',
  10: 'Ноябрь',
  11: 'Декабрь',
};

export const NAIL_FORM = 'nailForm';

@Component({
  selector: 'app-mobile-calendar',
  templateUrl: './mobile-calendar.component.html',
  styleUrls: ['./mobile-calendar.component.scss'],
  animations: [
    trigger('expand', [
      transition(':enter', [
        query(':self', [
          style({ height: 0, opacity: 0 }),
          animate('200ms ease-in', style({ height: '*', opacity: 1 })),
        ]),
      ]),
      transition(':leave', [
        query(':self', [
          animate('200ms ease-in',
            style({ height: 0, opacity: 0 }),
          ),
        ]),
      ]),
    ])
  ],
})
export class MobileCalendarComponent implements OnInit {

  public form: FormGroup;
  public rawDate: Date;
  public newDate: Date;
  public get date(): string {
    const date = this.newDate.getDate();
    return date < 10 ? '0' + date : String(date);
  }
  public get month(): number {
    return MONTH_ENUM[this.newDate.getMonth()];
  }
  // public get servicesPrice(): any {
  //   const services = this.form ? this.form.get('service').value.filter(service => service.selected) : [];
  //   return services.reduce((a, b) => a + b.price, 0);
  // }
  public step: number;
  public dateDelta: number;
  public margin: number;
  public fullWidth: number;
  constructor(
    private cdr: ChangeDetectorRef,
  ) {

    this.step = 10;
    this.dateDelta = 0;
    this.rawDate = new Date();
    this.newDate = this.rawDate;
    this.margin = 0;
    this.fullWidth = window.innerWidth;

  }

  ngOnInit() {
  }

  abs(num: number) {
    return Math.abs(num);
  }

  /* Pan Days*/
  panEndDay() {
    this.margin = 0;
    setTimeout(() => {
      this.rawDate = this.newDate;
      // this.form.controls.date.setValue(this.rawDate);
      this.cdr.detectChanges();
    }, 1);
  }

  panLeftDay(e) {
    this.dateDelta = -(e.deltaX / this.step) >> 0;
    this.newDate = addDays(this.rawDate, this.dateDelta);
    this.margin = e.deltaX;
  }

  panRightDay(e) {
    this.dateDelta = -(e.deltaX / this.step) >> 0;
    this.newDate = addDays(this.rawDate, this.dateDelta);
    this.margin = e.deltaX;
  }

  /* Pan Months*/
  panEndMonth() {
    this.margin = 0;
    setTimeout(() => {
      this.rawDate = this.newDate;
      this.cdr.detectChanges();
    }, 1);
  }

  panLeftMonth(e) {
    this.dateDelta = -(e.deltaX / this.step) >> 0;
    this.newDate = addMonths(this.rawDate, this.dateDelta);
    this.margin = e.deltaX;
  }

  panRightMonth(e) {
    this.dateDelta = -(e.deltaX / this.step) >> 0;
    this.newDate = addMonths(this.rawDate, this.dateDelta);
    this.margin = e.deltaX;
  }

  openUserForm() {
  }

  closeUserForm() {
  }

}
