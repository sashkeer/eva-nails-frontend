import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileCalendarComponent } from './mobile-calendar.component';

describe('MobileCalendarComponent', () => {
  let component: MobileCalendarComponent;
  let fixture: ComponentFixture<MobileCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
