import { ITime } from '../../shared/modals/add-event/add-event.component';

export const TIMES: ITime[] = [
  {
    time: '9:00 - 12:00',
    title: 'Утро',
    alias: 'morning',
    icon: 'flaticon-morning',
  },
  {
    time: '12:00 - 15:00',
    title: 'День',
    alias: 'afternoon',
    icon: 'flaticon-afternoon',
  },
  {
    time: '15:00 - 18:00',
    title: 'Вечер',
    alias: 'evening',
    icon: 'flaticon-evening',
  },
  {
    time: '18:00+',
    title: 'Ночь',
    alias: 'night',
    icon: 'flaticon-night',
  },
]
