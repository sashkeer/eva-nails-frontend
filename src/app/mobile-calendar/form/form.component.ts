import { Component, OnInit } from '@angular/core';
import { ITime } from '../../shared/modals/add-event/add-event.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/index';
import { TIMES } from './form.dictionary';
import { NAIL_FORM } from '../mobile-calendar.component';
import { delay, finalize, tap } from 'rxjs/operators';
import { IFullEvent, IService } from '../../$core/interfaces/event';
import { MeetingsService } from '../../+meetings/meetings.service';
import { LoadingService } from '../../services/loading/loading.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  public times: ITime[] = TIMES;

  public form: FormGroup;

  public get services(): any {
    return this.form ? this.form.get('service') : null;
  }

  public get selectedServices(): any {
    return this.form ? this.form.get('service').value.filter(service => service.selected) : [];
  }

  constructor(
    private meetingsService: MeetingsService,
    private loadingService: LoadingService,
    private fb: FormBuilder,
  ) {

    const oldForm = JSON.parse(localStorage.getItem(NAIL_FORM) || '{}');

    this.meetingsService.getServices()
      .subscribe((services: IService[]) => {
        const name = oldForm.name || '';
        const phone = oldForm.phone || '';
        const date = oldForm.date || new Date();
        const time = oldForm.time || 'afternoon';
        if (time) {
          this.times.forEach((t) => {
            if (t.alias === time) {
              t.selected = true;
            }
          });
        }
        const oldServices = oldForm.service || [];

        this.form = this.fb.group({
          name: this.fb.control(name, [Validators.required]),
          phone: this.fb.control(phone, [Validators.required]),
          service: this.fb.array(
            services.map((service: IService) => {
              const serviceObject = {};
              const wasSelected = !!oldServices.find(s => s.id === service.id && s.selected);
              service.selected = wasSelected;
              Object.keys(service).forEach((key: string) => {
                console.log(service[key]);
                serviceObject[key] = this.fb.control(service[key]);
              });
              return this.fb.group(serviceObject);
            }),
            [Validators.required]
          ),
          date: this.fb.control(date, [Validators.required]),
          time: this.fb.control(time, [Validators.required]),
        });
        this.form.valueChanges
          .pipe(
            tap(() => {
              localStorage.setItem(NAIL_FORM, JSON.stringify(this.form.value));
            }),
          )
          .subscribe();
      });
  }

  ngOnInit() {
  }

  closeUserForm() {

  }

  selectTime(time: ITime) {
    this.times.forEach((t: any) => {
      t.selected = false;
    });
    time.selected = true;
    this.form.get('time').setValue(time.alias);
  }

  public selectService(service: IService) {
    const control = this.services.controls
      .find((serviceControl: FormGroup) => serviceControl.value.id === service.id);
    control.controls.selected.setValue(!service.selected);
    service.selected = !service.selected;
  }
  public toggleServicesModal() {
    // this.servicesOpened = !this.servicesOpened;
  }

  public submitForm() {
    // this.form.controls.date.setValue(this.newDate);
    localStorage.setItem(NAIL_FORM, '');
    this.addEvent(this.form.value).subscribe();
  }

  public addEvent(event: IFullEvent): Observable<any> {
    return this.meetingsService
      .addEvent({
        ...event,
        service: event.service
          .filter((service: IService) => service.selected)
          .map((service: IService) => service.id),
      })
      .pipe(
        tap(() => this.loadingService.start()),
        delay(3000),
        finalize(() => {
          this.loadingService.stop();
          // ToDo go to calendar
         }),
      );
  }

}
