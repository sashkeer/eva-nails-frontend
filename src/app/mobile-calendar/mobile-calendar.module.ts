import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobileCalendarComponent } from './mobile-calendar.component';
import { RouterModule } from '@angular/router';
import { routes } from './mobile-calendar.routing';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { options } from '../app.module';
import { CheckboxModule } from '../shared/ui/checkbox/checkbox.module';

@NgModule({
  declarations: [MobileCalendarComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class MobileCalendarModule { }
