import { Routes } from '@angular/router';
import { MobileCalendarComponent } from './mobile-calendar.component';

export const routes: Routes = [
  {
    path: '',
    component: MobileCalendarComponent,
    pathMatch: 'full',
  },
];
