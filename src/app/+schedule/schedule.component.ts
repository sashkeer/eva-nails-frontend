import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { addMonths, isSameMonth, subMonths, addHours } from 'date-fns';
import { CalendarDateFormatter, CalendarEvent, CalendarMonthViewDay, CalendarUtils } from 'angular-calendar';
import * as moment from 'moment';
import { AddEventComponent } from '../shared/modals/add-event/add-event.component';
import { InformationComponent } from '../shared/modals/information/information.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IShortEvent, MeetingsService } from '../+meetings/meetings.service';
import { MatDialog } from '@angular/material';
import { Observable, of, Subject } from 'rxjs/index';
import { map } from 'rxjs/internal/operators';
import { IFullEvent, IService } from '../$core/interfaces/event';
import { RuDateFormatter } from '../+meetings/ru-date-provider';
import { MyCalendarUtils } from './calendar-utils';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: RuDateFormatter
    },
    {
      provide: CalendarUtils,
      useClass: MyCalendarUtils,
    },
  ]
})
export class ScheduleComponent implements OnInit {
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  public events$: Observable<CalendarEvent[]>;
  services: IService[] = [];
  viewDate: Date = new Date();
  locale = 'ru';
  minDate: Date = new Date();
  maxDate: Date = addMonths(new Date(), 2);
  addMonths = addMonths;
  subMonths = subMonths;
  refresh: Subject<any> = new Subject();
  public activeDayIsOpen = false;

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  constructor(
    private modal: NgbModal,
    public dialog: MatDialog,
    private meetingsService: MeetingsService,
  ) {
    this.events$ = of([]);
  }

  ngOnInit() {
    this.events$ = this.meetingsService.getPublicEvents(new Date(), 'month')
      .pipe(map((events: IShortEvent[]) => {
        return events.map(event => ({
          id: event.id,
          start: event.date,
          end: addHours(event.date, 2),
          title: 'Маникюр',
        }));
      }));
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach(day => {
      if (!this.dateIsValid(day.date) || this.dateIsFull(day.events)) {
        day.cssClass = 'cal-disabled';
      }
      if (this.dateIsToday(day.date)) {
        day.cssClass = 'cal-today';
      }
    });
  }

  dateIsFull(events: any): boolean {
    return events.length === 4;
    // && date <= this.maxDate;
  }

  dateIsValid(date: Date): boolean {
    return date >= this.minDate;
    // && date <= this.maxDate;
  }

  dateIsToday(date: Date): boolean {
    const today = moment(new Date()).clone().startOf('day');
    return moment(date).isSame(today, 'd');
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(event: IFullEvent): Observable<any> {
    return this.meetingsService.addEvent(event);
  }

  openDialog(date: Date): void {
    const addEventDialogRef = this.dialog.open(AddEventComponent, {
      width: '450px',
      data: {
        date,
        title: 'Запись на ' + moment(date).format('MMMM DD, YYYY'),
        description: 'Укажите ваше имя, фамилию, номер телефона, чтобы я могла с вами связаться, а также услугу которую хотете получить',
      },
    });

    addEventDialogRef.afterClosed().subscribe((result: IFullEvent) => {
      if (result) {
        this.addEvent(result).subscribe((data) => {
          this.ngOnInit();
          const infoDialogRef = this.dialog.open(InformationComponent, {
            width: '450px',
            data: {
              date,
              title: 'Спасибо!',
              description: 'Вы записаны на ' + moment(date).format('MMMM DD, YYYY'),
            },
          });
        });
      }
    });
  }

  dayClicked(event: { date: Date; events: CalendarEvent[] }): void {
    const { date, events } = event;
    // this.selectedEvents = events.map((ev) => ev.meta);
    this.viewDate = date;
    if (this.dateIsValid(date) || this.dateIsToday(date)) {
      if (!this.dateIsToday(date) && events.length < 4) {
        this.openDialog(date);
      }
      // this.events$ = this.updateEvents(this.viewDate);
      if (isSameMonth(date, this.viewDate)) {
        this.viewDate = date;
      }
    }
  }

}
