import { addMonths, addWeeks, endOfMonth } from 'date-fns';
import { CalendarUtils } from 'angular-calendar';
import { GetMonthViewArgs, MonthView } from 'calendar-utils';
import { Injectable } from '@angular/core';

@Injectable()
export class MyCalendarUtils extends CalendarUtils {
  getMonthView(args: GetMonthViewArgs): MonthView {
    args.viewStart = new Date();
    args.viewEnd = addWeeks(new Date(), 4);
    return super.getMonthView(args);
  }
}
