import { Component } from '@angular/core';
import { LoadingService } from './services/loading/loading.service';
import { Observable } from 'rxjs/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'eva-nails';
  public loading$: Observable<boolean>;

  constructor(
    private loadingService: LoadingService,
  ) {
    this.loading$ = this.loadingService.loading$;
  }

}
