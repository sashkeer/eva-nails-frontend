import { Routes } from '@angular/router';
import { LoginGuard } from './$core/guards/login/login.guard';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./+schedule/schedule.module').then(m => m.ScheduleModule),
  },
  {
    path: 'admin',
    redirectTo: 'admin/meetings',
    pathMatch: 'full',
  },
  {
    path: 'admin/login',
    loadChildren: () => import('./+login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'admin/meetings',
    canActivate: [LoginGuard],
    loadChildren: () => import('./+meetings/meetings.module').then(m => m.MeetingsModule),
  },
  {
    path: 'mobile-calendar',
    loadChildren: () => import('./mobile-calendar/mobile-calendar.module').then(m => m.MobileCalendarModule),
  },
];
