import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MeetingsModule } from './+meetings/meetings.module';
import { ConfirmationComponent } from './shared/modals/confirmation/confirmation.component';
import { AddEventComponent } from './shared/modals/add-event/add-event.component';
import { MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from './shared/angular-material/angular-material.module';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { EmojiModule } from '@ctrl/ngx-emoji-mart/ngx-emoji';
import { InformationComponent } from './shared/modals/information/information.component';
import { IConfig, NgxMaskModule } from 'ngx-mask';
export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  declarations: [
    AppComponent,
    ConfirmationComponent,
    AddEventComponent,
    InformationComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MeetingsModule,
    MatDialogModule,
    AngularMaterialModule,
    PickerModule,
    EmojiModule,
    NgxMaskModule.forRoot(options),
  ],
  providers: [],
  entryComponents: [
    ConfirmationComponent,
    AddEventComponent,
    InformationComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
