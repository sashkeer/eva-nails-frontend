import {
  Component,
  ViewChild,
  TemplateRef,
  OnInit, HostBinding,
} from '@angular/core';
import {
  isSameMonth,
  addMonths,
  subMonths,
} from 'date-fns';
import { Observable, of, Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarDateFormatter,
  CalendarEvent,
  CalendarMonthViewDay,
  CalendarView
} from 'angular-calendar';
import { RuDateFormatter } from './ru-date-provider';
import { MatDialog, MatPaginator } from '@angular/material';
import { AddEventComponent } from '../shared/modals/add-event/add-event.component';
import * as moment from 'moment';
import { InformationComponent } from '../shared/modals/information/information.component';
import { MeetingsService } from './meetings.service';
import { map, switchMap } from 'rxjs/internal/operators';
import { EventDataSource } from '../$core/data-sources/event.data.source';
import { IFullEvent, IService } from '../$core/interfaces/event';

const colors: any = {
  morning: {
    primary: '#FFEB3B',
    secondary: '#FFEB3B'
  },
  afternoon: {
    primary: '#FF5722',
    secondary: '#FF5722'
  },
  evening: {
    primary: '#3F51B5',
    secondary: '#3F51B5'
  },
  night: {
    primary: '#673AB7',
    secondary: '#673AB7'
  }
};

export enum CalendarMode {
  edit = 'edit',
  view = 'view',
}

export enum UserRole {
  admin = 'admin',
  client = 'client',
}

@Component({
  selector: 'app-meetings',
  templateUrl: './meetings.component.html',
  styleUrls: ['./meetings.component.scss'],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: RuDateFormatter
    }
  ]
})
export class MeetingsComponent implements OnInit {


  public dataSource: EventDataSource;
  @ViewChild(MatPaginator, {static: true}) public paginator: MatPaginator;
  public displayedColumns: string[];

  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;
  @HostBinding('class.dark-theme') darkThemeClass = false;

  CalendarView = CalendarView;

  viewDate: Date = new Date();
  locale = 'ru';
  minDate: Date = new Date();
  maxDate: Date = addMonths(new Date(), 2);
  addMonths = addMonths;
  subMonths = subMonths;

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  refresh: Subject<any> = new Subject();

  events$: Observable<CalendarEvent[]> = of([]);
  services: IService[] = [];

  private activeDayIsOpen = false;
  private mode: CalendarMode = CalendarMode.edit;
  private role: UserRole = UserRole.admin;

  constructor(
    private modal: NgbModal,
    public dialog: MatDialog,
    private meetingsService: MeetingsService,
  ) {

    this.displayedColumns = [
      'userName',
      'phone',
      'status',
      'time',
      'actions',
    ];
    this.dataSource = new EventDataSource(this.meetingsService);
  }

  ngOnInit() {
    // this.events = [];
    this.events$ = this.updateEvents(this.viewDate);
    this.meetingsService.getServices().subscribe(services => this.services = services);
  }

  getColorScore(color: string) {
    switch (color) {
      case 'morning':
        return 1;
      case 'afternoon':
        return 2;
      case 'evening':
        return 3;
      case 'night':
        return 4;
    }
  }

  dayClicked(event: { date: Date; events: CalendarEvent[] }): void {
    const { date } = event;
    // this.selectedEvents = events.map((ev) => ev.meta);
    this.viewDate = date;
    if (this.dateIsValid(date) || this.dateIsToday(date) || this.role === UserRole.admin) {
      // if (this.mode === CalendarMode.edit && !this.dateIsToday(date) && events.length < 4) {
      //   this.openDialog(date, events);
      // }
      this.events$ = this.updateEvents(this.viewDate);
      if (isSameMonth(date, this.viewDate)) {
        this.viewDate = date;
      }
    }
  }

  private updateEvents(date: Date): Observable<any> {
    this.dataSource.load(this.viewDate);
    return this.meetingsService.getEvents(date, 'day')
      .pipe(map((events: IFullEvent[]) => {
        // this.events = [];
        // events.forEach((e: IMeetingEvent) => {
        //   this.events.push({
        //     id: e.id,
        //     color: colors[e.time],
        //     time: e.time,
        //     start: new Date(e.date),
        //     end: new Date(e.date),
        //     title: e.user.name,
        //     meta: e,
        //   } as any);
        // });
        // this.events = this.events.sort((a: any, b: any) =>
        //   this.getColorScore(a.time) > this.getColorScore(b.time)
        //     ? 1
        //     : -1);
        // this.selectedEvents = this.events.map((ev) => ev.meta);
        // this.refresh.next();
        return events.sort((a: IFullEvent, b: IFullEvent) =>
          this.getColorScore(a.time) > this.getColorScore(b.time)
            ? 1
            : -1);
      }));
  }

  openDialog(date: Date, events: any[]): void {
    const addEventDialogRef = this.dialog.open(AddEventComponent, {
      width: '450px',
      data: {
        date,
        events,
        services: this.services,
        title: 'Запись на ' + moment(date).format('MMMM DD, YYYY'),
        description: 'Укажите ваше имя, фамилию, номер телефона, чтобы я могла с вами связаться, а также услугу которую хотете получить',
      },
    });

    addEventDialogRef.afterClosed().subscribe((result: IFullEvent) => {
      if (result) {
        this.addEvent(result).subscribe((data) => {
          this.ngOnInit();
          const infoDialogRef = this.dialog.open(InformationComponent, {
            width: '450px',
            data: {
              date,
              title: 'Спасибо!',
              description: 'Вы записаны на ' + moment(date).format('MMMM DD, YYYY'),
            },
          });
        });
      }
    });
  }

  // eventTimesChanged({
  //                     event,
  //                     newStart,
  //                     newEnd
  //                   }: CalendarEventTimesChangedEvent): void {
  //   this.events = this.events.map(iEvent => {
  //     if (iEvent === event) {
  //       return {
  //         ...event,
  //         start: newStart,
  //         end: newEnd
  //       };
  //     }
  //     return iEvent;
  //   });
  //   this.handleEvent('Dropped or resized', event);
  // }

  handleEvent(action: string, event: CalendarEvent): void {
    if (this.mode === CalendarMode.edit) {
      this.modalData = { event, action };
      this.modal.open(this.modalContent, { size: 'lg' });
    }
  }

  dateIsFull(events: any): boolean {
    return events.length === 4;
    // && date <= this.maxDate;
  }

  dateIsValid(date: Date): boolean {
    return date >= this.minDate;
    // && date <= this.maxDate;
  }

  dateIsToday(date: Date): boolean {
    const today = moment(new Date()).clone().startOf('day');
    return moment(date).isSame(today, 'd');
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach(day => {
      if (!this.dateIsValid(day.date) || this.dateIsFull(day.events)) {
        day.cssClass = 'cal-disabled';
      }
      if (this.dateIsToday(day.date)) {
        day.cssClass = 'cal-today';
      }
    });
  }

  addEvent(event: IFullEvent): Observable<any> {
    return this.meetingsService.addEvent(event);
  }

  // deleteEvent(eventToDelete: CalendarEvent) {
  //   this.events = this.events.filter(event => event !== eventToDelete);
  // }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

}
