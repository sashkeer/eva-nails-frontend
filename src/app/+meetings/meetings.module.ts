import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { MeetingsComponent } from './meetings.component';
import { MeetingsService } from './meetings.service';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import localeRu from '@angular/common/locales/ru';
import { HttpClientModule } from '@angular/common/http';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { PhonePipe } from '../../utils/pipes/phone';
import { routes } from './meetings.routing';

registerLocaleData(localeRu);

@NgModule({
  declarations: [
    MeetingsComponent,
    PhonePipe
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    NgbModalModule,
    HttpClientModule,
    AngularMaterialModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [MeetingsService],
})
export class MeetingsModule { }
