import { Routes } from '@angular/router';
import { MeetingsComponent } from './meetings.component';

export const routes: Routes = [
  {
    path: '',
    component: MeetingsComponent,
    pathMatch: 'full',
  },
];
