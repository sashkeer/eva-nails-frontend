import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { QueryService } from '../services/query/query.service';
import * as moment from 'moment';
import { ETime } from '../shared/modals/add-event/add-event.component';
import { IFullEvent } from '../$core/interfaces/event';

export interface IShortEvent {
  id: number;
  status: string;
  time: ETime;
  date: Date;
}

@Injectable()
export class MeetingsService {

  constructor(
    protected httpClient: HttpClient
  ) { }

  public addEvent(event: any): Observable<any> {
    return this.httpClient
      .post<any>(`${environment.url}/event`, event);
  }

  public getEvents(date: Date, period?: 'day' | 'month'): Observable<IFullEvent[]> {
    return this.httpClient
      .get<IFullEvent[]>(`${environment.url}/event${QueryService.paramsToQuery({
        period,
        date: moment(date).format('YYYY.MM.DD'),
      })}`);
  }

  public getPublicEvents(date: Date, period?: 'day' | 'month'): Observable<IShortEvent[]> {
    return this.httpClient
      .get<IShortEvent[]>(`${environment.url}/event/public${QueryService.paramsToQuery({
        period,
        date: moment(date).format('YYYY.MM.DD'),
      })}`);
  }

  public getServices(): Observable<any> {
    return this.httpClient
      .get(`${environment.url}/service`);
  }

}
