import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatCardModule, MatIconModule, MatListModule, MatSortModule, MatTableModule,
  MatToolbarModule, MatTooltipModule, MatDialogModule, MatRippleModule, MatCheckboxModule, MatFormFieldModule,
  MatInputModule, MatSelectModule, MatDividerModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatStepperModule, MatAutocompleteModule, MatExpansionModule,
} from '@angular/material';

const matModules = [
  MatTableModule,
  MatSortModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatCardModule,
  MatButtonModule,
  MatTooltipModule,
  MatDialogModule,
  MatRippleModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatDividerModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatAutocompleteModule,
  MatStepperModule,
  MatExpansionModule,
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...matModules,
  ],
  exports: [
    ...matModules,
  ]
})
export class AngularMaterialModule {}
