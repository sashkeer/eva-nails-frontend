import { Component, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatOptionSelectionChange } from '@angular/material';
import { IShortEvent, MeetingsService } from '../../../+meetings/meetings.service';
import { combineLatest, Observable } from 'rxjs/index';
import { tap } from 'rxjs/internal/operators';
import { IService } from '../../../$core/interfaces/event';

export type ETime = 'morning' | 'afternoon' | 'evening' | 'night';

export interface ITime {
  time: string;
  title: string;
  alias: ETime;
  icon: string;
  selected?: boolean;
}

// export interface IService {
//   id: number;
//   name: string;
//   description: string;
//   price: number;
//   deprecated: boolean;
//   selected?: boolean;
// }

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent {

  form: FormGroup;

  public times: ITime[] = [
    {
      time: '9:00 - 12:00',
      title: 'Утро',
      alias: 'morning',
      icon: 'flaticon-morning',
    },
    {
      time: '12:00 - 15:00',
      title: 'День',
      alias: 'afternoon',
      icon: 'flaticon-afternoon',
    },
    {
      time: '15:00 - 18:00',
      title: 'Вечер',
      alias: 'evening',
      icon: 'flaticon-evening',
    },
    {
      time: '18:00+',
      title: 'Ночь',
      alias: 'night',
      icon: 'flaticon-night',
    },
  ];

  selectTime(time: ITime) {
    this.times.forEach((t: any) => {
      t.selected = false;
    });
    time.selected = true;
    this.form.get('time').setValue(time.alias);
  }

  get services() {
    return this.form.get('service');
  }

  constructor(
    public dialogRef: MatDialogRef<AddEventComponent>,
    private fb: FormBuilder,
    private meetingsService: MeetingsService,
    @Inject(MAT_DIALOG_DATA) public data: {
      title: string,
      message: string,
      data?: any,
      date: Date,
      actions: any
    }) {
    combineLatest([
      this.fetchServices(),
      this.fetchEvents(data.date),
    ]).pipe(tap(([services, events]) => {
      this.times = this.times.filter(time => !events.some(event => time.alias === event.time));
      this.form = this.fb.group({
        name: this.fb.control('', [Validators.required]),
        phone: this.fb.control('', [Validators.required]),
        service: this.fb.array(
          services.map((service: IService) => {
            const serviceObject = {};
            Object.keys(service).forEach((key: string) => {
              serviceObject[key] = this.fb.control(service[key]);
            });
            return this.fb.group(serviceObject);
          }),
          [Validators.required]
        ),
        date: this.fb.control(data.date, [Validators.required]),
        time: this.fb.control('', [Validators.required]),
      });
    })).subscribe();
  }

  private fetchServices(): Observable<IService[]> {
    return this.meetingsService.getServices();
  }

  private fetchEvents(date: Date): Observable<IShortEvent[]> {
    return this.meetingsService.getPublicEvents(date, 'day');
  }

  public selectService(event: MatOptionSelectionChange, value: any) {
    value.value.selected = event.source.selected;
  }

  public prepareForm() {
    return {
      ...this.form.value,
      service: this.form.value.service
        .filter(x => x.selected)
        .map(x => x.id),
    };
  }


}
