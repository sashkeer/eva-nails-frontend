import { Component, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { getRandomInt } from '../../../helper';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent {

  reactionMessage: string;
  answers: string[] = ['Ясно', 'Понятно', 'Ок.', 'Поняла, приняла', 'Ясненько', 'Прекрасно',
    'Здорово.', 'Чудно', 'Ладно', 'Отлично', 'Славно', 'Превосходно', 'Великолепно',
    'Классно', 'Прекрасно'];

  constructor(
    public dialogRef: MatDialogRef<InformationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { title: string, description: string, data?: any, actions: any }) {
    this.reactionMessage = this.answers[getRandomInt(0, this.answers.length)];
  }

}
