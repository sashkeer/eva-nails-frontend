import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { tap } from 'rxjs/internal/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public username: string;
  public password: string;

  constructor(private loginService: LoginService, private router: Router) {
    this.username = '';
    this.password = '';
  }

  ngOnInit() {}

  login() {
    this.loginService.login(this.username, this.password)
      .pipe(tap((result: boolean) => {
        if (result) {
          this.router.navigate(['/', 'admin', 'meetings']);
        } else {
          // Todo throw message
        }
      }))
      .subscribe();
  }

}
