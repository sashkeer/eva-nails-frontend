import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    protected httpClient: HttpClient
  ) { }

  public async isLoggedIn(): Promise<boolean> {
    return this.httpClient
      .get<boolean>(`${environment.url}/auth/validate`).toPromise();
  }

  public login(username: string, password: string): Observable<boolean> {
    return this.httpClient
      .post<boolean>(`${environment.url}/auth/login`, { username, password });
  }

  public logout(): Observable<void> {
    return this.httpClient
      .get<void>(`${environment.url}/auth/logout`);
  }
}
