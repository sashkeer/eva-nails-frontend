import { Injectable } from '@angular/core';

@Injectable()
export class QueryService {
  public static paramsToQuery(params?: object): string {
    if (!params) {
      return '';
    }
    const query = Object.keys(params).map((key: string) => {
      const value = params[key];
      if (typeof value === 'object') {
        return Object.keys(value).map((subkey: string) => `${key}[${subkey}]=${value[subkey]}`).join('&');
      }
      return `${key}=${params[key]}`;
    }).join('&');
    return query === '' ? query : `?${query}`;
  }
}
