import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs/index';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  private loading: BehaviorSubject<boolean>;
  public loading$: Observable<boolean>;

  constructor() {
    this.loading = new BehaviorSubject<boolean>(false);
    this.loading$ = this.loading.asObservable();
  }

  public start() {
    this.loading.next(true);
  }

  public stop() {
    this.loading.next(false);
  }
}
