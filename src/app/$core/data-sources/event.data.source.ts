import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/internal/operators';
import { IFullEvent } from '../interfaces/event';
import { MeetingsService } from '../../+meetings/meetings.service';

export class EventDataSource implements DataSource<IFullEvent> {

  protected entitySubject = new BehaviorSubject<IFullEvent[]>([]);
  protected loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public countSubject = new BehaviorSubject<number>(0);

  constructor(protected service: MeetingsService) {}

  public connect(collectionViewer: CollectionViewer): Observable<IFullEvent[]> {
    return this.entitySubject.asObservable();
  }

  public disconnect(collectionViewer: CollectionViewer): void {
    this.entitySubject.complete();
    this.loadingSubject.complete();
  }

  public load(date: Date) {
    this.loadingSubject.next(true);
    this.service.getEvents(date, 'day').pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe((response: IFullEvent[]) => {
        this.entitySubject.next(response);
        this.countSubject.next(response.length);
      });
  }
}
