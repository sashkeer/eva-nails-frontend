
export interface IFullEvent {
  id: number;
  status: string;
  time: string;
  date: string;
  createDate: string;
  updateDate: string;
  user: IUser;
  service: IService[];
}

export interface IUser {
  id: number;
  ip: string;
  name: string;
  phone: string;
  role: string;
  createDate: string;
  updateDate: string;
}

export interface IService {
  id: number;
  name: string;
  description?: string;
  price: number;
  deprecated: boolean;
  selected?: boolean;
  createDate: string;
  updateDate: string;
}
